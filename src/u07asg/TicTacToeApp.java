package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/**
 * A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private TicTacToe ttt;
    private JButton[][] board = new JButton[3][3];
    private JButton exit = new JButton("Exit");
    private JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;

    private boolean againstComputer;
    private final JButton againstComputerButton = new JButton("vs cpu");
    private final JButton againstPlayerButton = new JButton("VS PLAYER");

    private void changeTurn() {
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt = ttt;
        initPane();
    }

    private void humanMove(int i, int j) {
        this.move(i, j);
    }

    private void computerMove() {
        Index idxToTick = Index.pickIndex(ttt.getRiskyIndexes(Strategy.ATTACK), board, Strategy.ATTACK, ttt);
        this.move(idxToTick.getI(), idxToTick.getJ());
    }

    private void move(int i, int j) {
        if (ttt.move(turn, i, j)) {
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            moves++;
            changeTurn();
            if (moves > 3) {
                System.out.println("X winning count: " + ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: " + ttt.winCount(turn, Player.PlayerO));
            }
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()) {
            exit.setText(victory.get() + " won!");
            finished = true;
            return;
        }
        this.checkCompleted();
    }

    private boolean checkCompleted() {
        if (ttt.checkCompleted()) {
            exit.setText("Even!");
            finished = true;
            return true;
        }
        return false;
    }

    private void initPane() {
        board = new JButton[3][3];
        exit = new JButton("Exit");
        finished = false;
        turn = Player.PlayerX;
        moves = 0;

        frame.setLayout(new BorderLayout());

        JPanel b = new JPanel(new GridLayout(3, 3));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                final int i2 = i;
                final int j2 = j;
                board[i][j] = new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> {
                    if (!finished) {
                        humanMove(i2, j2);
                        if (this.againstComputer && !finished) {
                            computerMove();
                        }
                    }
                });
            }
        }
        JPanel s = new JPanel(new FlowLayout());
        s.add(againstPlayerButton);
        againstPlayerButton.addActionListener(e -> {
            this.resetBtn(false, this.againstComputerButton, this.againstPlayerButton);
            this.resetBoard();
        });
        s.add(againstComputerButton);
        againstComputerButton.addActionListener(e -> {
            this.resetBtn(true, this.againstPlayerButton, this.againstComputerButton);
            this.resetBoard();
        });
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));

        frame.add(BorderLayout.CENTER, b);
        frame.add(BorderLayout.SOUTH, s);
        frame.setSize(200, 230);
        frame.setVisible(true);
    }

    private void resetBtn(boolean isAgainstComputer, JButton lowerBtn, JButton upperBtn){
        this.againstComputer = isAgainstComputer;
        lowerBtn.setText(lowerBtn.getText().toLowerCase());
        upperBtn.setText(upperBtn.getText().toUpperCase());
    }

    private void resetBoard() {
        this.ttt = new TicTacToeImpl("src/u07asg/ttt.pl");
        this.frame.getContentPane().removeAll();
        this.initPane();
    }

    public static void main(String[] args) {
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
