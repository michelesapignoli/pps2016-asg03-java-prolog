package u07asg

import u07asg.Scala2P._
import alice._
import alice.tuprolog.{Struct, Theory}
import java.io.FileInputStream
import java.util.Optional

import scala.collection.mutable.{Buffer}
import collection.JavaConverters._

/**
  *
  */
class TicTacToeImpl(fileName: String) extends TicTacToe {
  var pippo: Any = _

  implicit private def playerToString(player: Player): String = player match {
    case Player.PlayerX => "p1"
    case _ => "p2"
  }

  implicit private def stringToPlayer(s: String): Player = s match {
    case "p1" => Player.PlayerX
    case _ => Player.PlayerO
  }

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
  createBoard()

  override def createBoard() = {
    val goal = "retractall(board(_)),create_board(B),assert(board(B))"
    solveWithSuccess(engine, goal)
  }

  override def getBoard() = {
    val term = solveOneAndGetTerm(engine, "board(B)", "B").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.toString).map {
      case "null" => Optional.empty[Player]()
      case s => Optional.of[Player](s)
    }.to[Buffer].asJava
  }

  override def checkCompleted() = {
    val goal = "board(B),final(B,_)"
    solveWithSuccess(engine, goal)
  }

  override def checkVictory() = {
    val goal = "board(B),final(B,p1)"
    val goal2 = "board(B),final(B,p2)"
    if (solveWithSuccess(engine, goal)) Optional.of(Player.PlayerX)
    else if (solveWithSuccess(engine, goal2)) Optional.of(Player.PlayerO)
    else Optional.empty()
  }

  /**
    * Method based on a new goal declared in .ttt
    *
    * @return list of indexes of the risky cell(=two symbols next to each other or separated by a single space)
    * - depending on the Strategy(ATTACK/DEFENSE) -
    * to tick - or already ticked
    */
  override def getRiskyIndexes(strategy: Strategy) = {
    val goal = if(strategy == Strategy.DEFENSE) "board(B),findall_movepatt(B,p1,I,L)" else "board(B),findall_movepatt(B,p2,I,L)"
    var term = solveOneAndGetTerm(engine, goal, "L").asInstanceOf[Struct]
    val iterator = term.listIterator()
    iterator.asScala.toList.map(_.asInstanceOf[alice.tuprolog.Int]).map {
      case i => i.intValue().asInstanceOf[Integer]
    }.to[Buffer].asJava

  }

  override def move(player: Player, i: Int, j: Int): Boolean = {
    val goal = s"board(B), next_board(B,${playerToString(player)},B2)"
    val nextboard = (for {
      term <- engine(goal).map(extractTerm(_, "B2"))
      elem = term.asInstanceOf[Struct].listIterator().asScala.toList(i + 3 * j)
      if (elem.toString == playerToString(player))
    } yield term).headOption
    if (nextboard isEmpty) return false
    val goal2 = s"retractall(board(_)), assert(board(${nextboard.get.toString}))"
    solveWithSuccess(engine, goal2)
  }

  override def toString =
    solveOneAndGetTerm(engine, "board(B)", "B").toString

  override def winCount(current: Player, winner: Player): Int = {
    val goal = s"board(B), statistics(B,${playerToString(current)},${playerToString(winner)},Count)"
    solveOneAndGetTerm(engine, goal, "Count").asInstanceOf[tuprolog.Int].intValue()
  }
}
