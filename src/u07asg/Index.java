package u07asg;

import javax.swing.*;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by sapi9 on 30/04/2017.
 */
public class Index {
    private int i;
    private int j;

    public Index(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public Index(JButton[][] board) {
        int i, j;
        do {
            i = ThreadLocalRandom.current().nextInt(0, board.length);
            j = ThreadLocalRandom.current().nextInt(0, board.length);
        } while (board[i][j].getText() != "");
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return this.i;
    }

    public int getJ() {
        return this.j;
    }

    public static Index pickIndex(List<Integer> indexes, JButton[][] board, Strategy strategy, TicTacToe ttt){
        int i,j;
        for(Integer index:indexes){
            i = index/board.length;
            j = index%board.length;
            if(board[i][j].getText() == ""){
                return new Index(i,j);
            }
        }
        /**
         * If the index is found in ATTACK strategy (2 cells of my symbol next to each other or separated by a single space)
         *  then it's returned.
         * Else, a new call of the method is performed with DEFENSE strategy. If the index is found, returned, else random.
         */
        return strategy == Strategy.DEFENSE ? new Index(board)
                : pickIndex(ttt.getRiskyIndexes(Strategy.DEFENSE) ,board, Strategy.DEFENSE,ttt);
    }
}
